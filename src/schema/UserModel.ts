import mongoose, { Document } from "mongoose";
import { TimeStampFields } from "../interfaces";
import { ISubCategoryModelSchema } from "./SubCategoryModel";

const Schema = mongoose.Schema;

export enum UserTypeEnum {
  SP_IND = "SP-IND",
  SP_COM = "SP-COM",
  SR_IND = "SR-IND",
  SR_COM = "SR-COM",
  ADMIN = "ADMIN",
}

export interface IUserModel {
  userType: UserTypeEnum;
  firstName: string;
  lastName: string;
  email: string;
  NIDA: number;
  vocationTraining: "Yes" | "No";
  experience: number;
  companyName: string;
  contactPerson: string;
  jobTitle: string;
  phoneNumber: string;
  BRELA: number;
  noOfEmpoyees: number;
  region: string;
  district: string;
  ward: string;
  streetAddress: string;
  password: string;
  skills:
    | Array<ISubCategoryModelSchema["_id"]>
    | Array<ISubCategoryModelSchema>;
  documentsUrl: string[];
  profilePicUrl: string;
  isActive: boolean;
  isEmailConfirm: boolean;
  jobsCanBuy: number;
  refPersonName: string;
  refPhoneNumber: string;
  expDetails: string;
  forgetPassword: boolean;
}

export interface IUserModelSchema
  extends IUserModel,
    TimeStampFields,
    Document {}

const UserSchema = new Schema(
  {
    userType: {
      type: String,
      required: true,
      enum: ["SP-IND", "SP-COM", "ADMIN", "SR-IND", "SR-COM"],
    },

    firstName: {
      type: String,
      default: null,
    },

    lastName: {
      type: String,
      default: null,
    },

    email: {
      type: String,
      required: false,
      unique: true,
    },

    isEmailConfirm: {
      type: Boolean,
      default: false,
    },

    NIDA: {
      type: Number,
      default: 0,
    },

    vocationTraining: {
      type: String,
      default: "No",
      enum: ["Yes", "No"],
    },

    experience: {
      type: Number,
      default: 0,
    },

    companyName: {
      type: String,
      default: null,
    },

    contactPerson: {
      type: String,
      default: null,
    },

    jobTitle: {
      type: String,
      default: null,
    },

    phoneNumber: {
      type: String,
      required: true,
      unique: true,
    },

    BRELA: {
      type: Number,
      default: 0,
    },

    noOfEmpoyees: {
      type: Number,
      default: 0,
    },

    region: {
      type: String,
      required: true,
    },

    district: {
      type: String,
      required: true,
    },

    ward: {
      type: String,
      required: true,
    },

    streetAddress: {
      type: String,
      required: true,
    },

    password: {
      type: String,
      required: true,
    },

    skills: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "sub-category",
        default: null,
      },
    ],

    documentsUrl: [
      {
        type: String,
        default: null,
      },
    ],

    profilePicUrl: {
      type: String,
      default: "",
    },

    jobsCanBuy: {
      type: Number,
      default: 3,
    },

    refPersonName: {
      type: String,
      default: "",
    },

    refPhoneNumber: {
      type: String,
      default: "",
    },

    expDetails: {
      type: String,
      default: "",
    },

    isActive: {
      type: Boolean,
      default: true,
    },

    forgetPassword: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true },
);

export const UserModel = mongoose.model<IUserModelSchema>("users", UserSchema);
