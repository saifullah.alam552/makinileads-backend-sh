import mongoose, { Document } from "mongoose";
import { TimeStampFields } from "../interfaces";
import { ISubCategoryModelSchema } from "./SubCategoryModel";
import { IUserModelSchema } from "./UserModel";

const Schema = mongoose.Schema;

export enum JobStatusEnum {
  CONTACTED = "CONTACTED",
  COMPLETED = "COMPLETED",
  REPOSTED = "REPOSTED",
  OPEN = "OPEN",
  PURCHASED = "PURCHASED",
}

export enum JobPurchaseType {
  EXCLUSIVE = "EXCLUSIVE",
  GENERAL = "GENERAL",
  NONE = "NONE",
}

export interface IJobModel {
  jobTitle: string;
  jobDescription: string;
  jobAmount: number;
  skills:
    | Array<ISubCategoryModelSchema["_id"]>
    | Array<ISubCategoryModelSchema>;
  region: string;
  district: string;
  ward: string;
  streetAddress: string;
  postedBy: IUserModelSchema["_id"] | IUserModelSchema;
  purchasedBy: Array<IUserModelSchema["_id"]> | Array<IUserModelSchema>;
  isActive: boolean;
  jobStatus: JobStatusEnum;
  jobPurchaseType: JobPurchaseType;
}

export interface IJobModelSchema extends IJobModel, TimeStampFields, Document {}

let JobSchema = new Schema(
  {
    jobTitle: {
      type: String,
      required: true,
      unique: true,
    },

    jobDescription: {
      type: String,
      required: true,
      unique: true,
    },

    skills: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "sub-category",
        required: true,
      },
    ],

    jobAmount: {
      type: Number,
      required: true,
    },

    region: {
      type: String,
      required: true,
    },

    district: {
      type: String,
      required: true,
    },

    ward: {
      type: String,
      required: true,
    },

    streetAddress: {
      type: String,
      required: true,
    },

    postedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
      required: true,
    },

    purchasedBy: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        default: null,
      },
    ],

    jobStatus: {
      type: String,
      default: "OPEN",
      enum: ["CONTACTED", "COMPLETED", "REPOSTED", "OPEN", "PURCHASED"],
    },

    jobPurchaseType: {
      type: String,
      default: "NONE",
      enum: ["EXCLUSIVE", "GENERAL", "NONE"],
    },

    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

export const JobModel = mongoose.model<IJobModelSchema>("jobs", JobSchema);
