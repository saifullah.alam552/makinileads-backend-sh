import mongoose, { Document } from "mongoose";
import { TimeStampFields } from "../interfaces";
import logger from "../logger";
import { ICategoryModelSchema } from "./CategoryModel";
import { IJobModelSchema, JobModel } from "./JobModel";
import { IUserModel, IUserModelSchema, UserModel } from "./UserModel";

const Schema = mongoose.Schema;

export interface ISubCategoryModel {
  title: string;
  category: ICategoryModelSchema["_id"] | ICategoryModelSchema;
  isActive: boolean;
}

export interface ISubCategoryModelSchema
  extends ISubCategoryModel,
    TimeStampFields,
    Document {}

let SubCategorySchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },

    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "category",
      required: true,
    },

    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

SubCategorySchema.post("findOneAndDelete", (document) => {
  const skillId = document._id;

  UserModel.find({ skills: { $in: [skillId] } })
    .then((users: IUserModelSchema[]) => {
      Promise.all(
        users.map((user: IUserModelSchema) =>
          UserModel.findOneAndUpdate(
            { _id: user._id },
            { $pull: { skills: skillId } },
          ),
        ),
      ).catch((err: any) =>
        logger.error(
          `ERROR | SUB CATEGEGORY DELETE | USER UPDATE SKILLS & DELETE | ${err}`,
        ),
      );
    })
    .catch((err: any) =>
      logger.error(
        `ERROR | SUB CATEGEGORY DELETE | USER UPDATE SKILLS & DELETE | ${err}`,
      ),
    );

  JobModel.find({ skills: { $in: [skillId] } })
    .then((jobs: IJobModelSchema[]) => {
      Promise.all(
        jobs.map((job: IJobModelSchema) =>
          JobModel.findOneAndUpdate(
            { _id: job._id },
            { $pull: { skills: skillId } },
          ),
        ),
      ).catch((err: any) =>
        logger.error(
          `ERROR | SUB CATEGEGORY DELETE | JOB UPDATE SKILLS & DELETE | ${err}`,
        ),
      );
    })
    .catch((err: any) =>
      logger.error(
        `ERROR | SUB CATEGEGORY DELETE | JOB UPDATE SKILLS & DELETE | ${err}`,
      ),
    );
});

export const SubCategoryModel = mongoose.model<ISubCategoryModelSchema>(
  "sub-category",
  SubCategorySchema,
);
