import mongoose, { Document } from "mongoose";
import { TimeStampFields } from "../interfaces";
import logger from "../logger";
import { JobModel } from "./JobModel";
import { SubCategoryModel } from "./SubCategoryModel";
import { UserModel } from "./UserModel";

const Schema = mongoose.Schema;
export interface ICategoryModel {
  title: string;
  isActive: boolean;
}
export interface ICategoryModelSchema
  extends ICategoryModel,
    TimeStampFields,
    Document {}

let CategorySchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },

    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

// CategorySchema.post("findOneAndDelete", (document) => {
//   const catId = document._id;

//   SubCategoryModel.find({ category: catId })
//     .then((sub) => {
//       Promise.all(
//         sub.map((d) => {
//           console.log(d._id);
//           SubCategoryModel.findOneAndDelete({ _id: d._id });
//         }),
//       ).catch((err) =>
//         logger.error(`ERROR | CATEGORY DELETE | SUB CATEGORY DELETE | ${err}`),
//       );
//     })
//     .catch((err) =>
//       logger.error(`ERROR | CATEGORY DELETE | SUB CATEGORY DELETE | ${err}`),
//     );
// });

export const CategoryModel = mongoose.model<ICategoryModelSchema>(
  "category",
  CategorySchema,
);
