import mongoose, { Document } from "mongoose";
import { TimeStampFields } from "../interfaces";

const Schema = mongoose.Schema;

export interface IUserJobAcitivyModel {}

export interface IUserJobActivitySchema
  extends IUserJobAcitivyModel,
    TimeStampFields,
    Document {}

const UserJobActivitySchema = new Schema({}, { timestamps: true });

export const UserJobAcitivyModel = mongoose.model<IUserJobActivitySchema>(
  "user-job-activity",
  UserJobActivitySchema,
);
