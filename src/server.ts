import cors from "cors";
import express, { Request, Response } from "express";
import useragent from "express-useragent";
import mongoose from "mongoose";
import morgan from "morgan";
import { RequestWithUserID } from "./interfaces";
import logger, { LoggerStream } from "./logger";
import { headers } from "./middlewares";
import routes from "./routes";
import { MONGODB_URI } from "./utils";

morgan.token("userId", (req: RequestWithUserID) => {
  return req.userId + " | " + req.userType;
});

morgan.token("req_origin", (req: RequestWithUserID) => {
  return req.get("origin");
});

morgan.token("agentinfo", (req: RequestWithUserID) => {
  return (
    req.useragent.platform +
    " | " +
    req.useragent.os +
    " | " +
    req.useragent.browser +
    " " +
    req.useragent.version +
    " | " +
    req.useragent.source
  );
});

// MongoDB / Mongoose connection options
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: false,
  poolSize: 10,
  serverSelectionTimeoutMS: 5000,
  socketTimeoutMS: 45000,
};

// MongoDB Connection & init Express App
mongoose
  .connect(MONGODB_URI, options)
  .then(() => {
    logger.info("DB Connected");

    const app = express();

    // Express URL Eencoded middleware.
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.disable("x-powered-by");
    app.use(useragent.express());
    app.use(headers);
    app.use(cors());

    // logging middleware
    app.use(
      morgan(
        `:method | :req_origin :url | :response-time ms | :remote-addr | HTTP/:http-version | :status | :res[content-length] | :userId | :agentinfo`,
        { stream: new LoggerStream() },
      ),
    );

    // app.use(isAuth)

    // all routes
    app.use("/api", routes, cors());

    // global not found
    app.use((req: Request, res: Response) => {
      res.status(404).json({
        message: ["Request resource not found."],
        url: req.originalUrl,
      });
    });

    // setting port
    const port = process.env.PORT || 5000;

    // starting server
    app.listen(port, () => {
      logger.info(`App started on port ${port}`);
    });
  })
  .catch((err: any) => {
    logger.error(`Error Connecting Database... ${err}`);
  });
