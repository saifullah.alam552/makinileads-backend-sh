import {
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsEnum,
  IsNumber,
  IsString,
  MaxLength,
} from "class-validator";
import {
  IJobModel,
  IJobModelSchema,
  JobPurchaseType,
  JobStatusEnum,
} from "../schema/JobModel";
import { ISubCategoryModelSchema } from "../schema/SubCategoryModel";
import { IUserModelSchema } from "../schema/UserModel";

export class JobDto implements IJobModel {
  @IsString()
  jobTitle: string;

  @IsString()
  @MaxLength(2500)
  jobDescription: string;

  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  skills: Array<ISubCategoryModelSchema["_id"]>;

  @IsNumber({ allowInfinity: false, allowNaN: false, maxDecimalPlaces: 2 })
  jobAmount: number;

  @IsString()
  region: string;

  @IsString()
  district: string;

  @IsString()
  ward: string;

  @IsString()
  streetAddress: string;

  @IsString()
  postedBy: IUserModelSchema["_id"];

  isActive: boolean = true;
  jobPurchaseType: JobPurchaseType = JobPurchaseType.NONE;
  jobStatus: JobStatusEnum = JobStatusEnum.OPEN;
  purchasedBy: Array<IUserModelSchema["_id"]>;
}

export class JobPurchaseDto {
  @IsString()
  jobId: IJobModelSchema["_id"];

  @IsEnum(JobPurchaseType)
  jobPurchaseType: JobPurchaseType;
}

export class JobStatusDto {
  @IsEnum(JobStatusEnum)
  jobStatus: JobStatusEnum;

  @IsBoolean()
  isActive: boolean;
}
