import { IsString } from "class-validator";
import { ICategoryModelSchema } from "../schema/CategoryModel";

export class CategoryDto {
  @IsString()
  title: string;
  isActive: boolean = true;
}

export class SubCategoryDto {
  @IsString()
  title: string;

  @IsString()
  category: ICategoryModelSchema["_id"];

  isActive: boolean = true;
}
