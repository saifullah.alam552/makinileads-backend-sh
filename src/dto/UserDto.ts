import { IsEmail, IsEnum, IsString, MinLength } from "class-validator";
import { ISubCategoryModelSchema } from "../schema/SubCategoryModel";
import { IUserModel, UserTypeEnum } from "../schema/UserModel";

export class UserDto implements IUserModel {
  @IsEnum(UserTypeEnum)
  userType: UserTypeEnum;

  firstName: string;

  lastName: string;

  // @IsEmail()
  email: any;

  NIDA: number;

  vocationTraining: "Yes" | "No";

  experience: number;

  companyName: string;

  contactPerson: string;

  jobTitle: string;

  @IsString()
  phoneNumber: string;

  BRELA: number;

  noOfEmpoyees: number;

  @IsString()
  region: string;

  @IsString()
  district: string;

  @IsString()
  ward: string;

  @IsString()
  streetAddress: string;

  @IsString()
  @MinLength(6)
  password: string;

  confirmPassword: string;
  skills: Array<ISubCategoryModelSchema["_id"]> = [];
  documentsUrl: string[] = [];
  profilePicUrl: string = "";
  isActive: boolean = true;
  isEmailConfirm: boolean = false;
  jobsCanBuy: number = 3;
  refPersonName: string = "";
  refPhoneNumber: string = "";
  expDetails: string = "";
  forgetPassword: boolean = false;
}
