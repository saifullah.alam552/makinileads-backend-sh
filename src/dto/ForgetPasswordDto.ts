import { IsString } from "class-validator";

export class ForgetPasswordDto {
  @IsString()
  username: string;
}
