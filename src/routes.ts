import express from "express";
import CategoryController from "./controllers/CategoryController";
import JobController from "./controllers/JobController";
import UserController from "./controllers/UserControllers";
import { CategoryDto, SubCategoryDto } from "./dto/CategoryDto";
import { ForgetPasswordDto } from "./dto/ForgetPasswordDto";
import { JobDto, JobPurchaseDto, JobStatusDto } from "./dto/JobDto";
import { LoginDto } from "./dto/LoginDto";
import { UserDto } from "./dto/UserDto";
import { dtoValid, isAuth } from "./middlewares";

const routes = express.Router();      

routes.post("/register", dtoValid(UserDto), UserController.addUser);
routes.post(
  "/checkEmailPhone",
  dtoValid(UserDto),     
  UserController.checkEmailPhone,
);
routes.post("/login", dtoValid(LoginDto), UserController.login);
routes.post(
  "/forgetPassword",
  dtoValid(ForgetPasswordDto),
  UserController.forgetPassword,
);
routes.get("/confirmEmail/:id", UserController.confirmEmail);
routes.post(
  "/changeIsActive/:id/:active",
  isAuth,
  UserController.changeIsActive,
);
routes.post("/changeUserJobs/:id/:jobs", isAuth, UserController.changeUserJobs);
routes.get("/getUserInfo", isAuth, UserController.getUserInfo);
routes.post("/changePassword", isAuth, UserController.changePassword);
routes.post("/updateUser/:id", isAuth, UserController.updateUser);
routes.get("/getUsers", isAuth, UserController.getUsers);

routes.post(
  "/addCategory",
  dtoValid(CategoryDto),
  CategoryController.addCategory,
);
routes.post(
  "/editCategory/:id",
  dtoValid(CategoryDto),
  CategoryController.editCategory,
);
routes.post("/deleteCategory/:id", CategoryController.deleteCategory);

routes.get("/getCategories", CategoryController.getCategories);
routes.post(
  "/addSubCategory",
  dtoValid(SubCategoryDto),
  CategoryController.addSubCategory,
);
routes.post(
  "/editSubCategory/:id",
  dtoValid(SubCategoryDto),
  CategoryController.editSubCategory,
);

routes.post("/deleteSubCategory/:id", CategoryController.deleteSubCategory);

routes.get("/getSubCategories", CategoryController.getSubCategories);

routes.post("/addJob", dtoValid(JobDto), JobController.addJob);
routes.post("/editJob/:id", dtoValid(JobDto), isAuth, JobController.editJob);
routes.post(
  "/changeJobStatus/:id",
  dtoValid(JobStatusDto),
  isAuth,
  JobController.changeJobStatus,
);
routes.post(
  "/purchaseJob",
  isAuth,
  dtoValid(JobPurchaseDto),
  JobController.purchaseJob,
);
routes.get("/getJobs", JobController.getJobs);

routes.post("/deleteJob/:id", isAuth, JobController.deleteJob);

// test api
routes.get("/", (req: express.Request, res: express.Response) => {
  res.status(200).json({
    message: "api is working... makini leads",
    date: new Date().toUTCString(),
  });
});

export default routes;
