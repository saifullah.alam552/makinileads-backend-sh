import { Request } from "express";
import { UserTypeEnum } from "./schema/UserModel";

export interface RequestWithUserID extends Request {
  userId: string;
  userType: UserTypeEnum;
}

export interface TokenData {
  expiresIn: number;
  accessToken: string;
}

export interface DataStoredInToken {
  userId: string;
  userType: UserTypeEnum;
}

export interface TimeStampFields {
  createdAt: string;
  updatedAt: string;
}
