import * as bcrypt from "bcryptjs";
import { isEmail } from "class-validator";
import { Response } from "express";
import qs from "qs";
import { randomPassword } from "secure-random-password";
import url from "url";
import { ChangePasswordDto } from "../dto/ChangePasswordDto";
import { ForgetPasswordDto } from "../dto/ForgetPasswordDto";
import { LoginDto } from "../dto/LoginDto";
import { UserDto } from "../dto/UserDto";
import { RequestWithUserID } from "../interfaces";
import logger from "../logger";
import { resmessage } from "../resmessage";
import { IUserModelSchema, UserModel, UserTypeEnum } from "../schema/UserModel";
import {
  cloudinary,
  createToken,
  fronendUrl,
  getPublicIdFromUrl,
  hostUrl,
  removedfromArray,
  sendEmail,
} from "../utils";

const UserController = {
  addUser: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: UserDto = req.body;

      const hashPassword = await bcrypt.hash(data.password, 12);

      const jobs =
        data.userType === UserTypeEnum.ADMIN ||
        data.userType === UserTypeEnum.SR_COM ||
        data.userType === UserTypeEnum.SR_IND
          ? 0
          : 3;

      data = {
        ...data,
        password: hashPassword,
        jobsCanBuy: jobs,
        // isEmailConfirm: true, // for test purpose.
      };

      const result = await new UserModel(data).save();

      const name =
        result.userType === UserTypeEnum.SP_COM ||
        result.userType === UserTypeEnum.SR_COM
          ? result.contactPerson
          : `${result.firstName} ${result.lastName}`;

          if(result.email) {
            const email = await sendEmail({
              toEmail: result.email,
              toName: name,
              subject: "Confirm Email",
              htmlBody: `<b>Email Confirmation Link</b><br/><a href="${hostUrl(
                req,
              )}/api/confirmEmail/${result._id}">${hostUrl(req)}/api/confirmEmail/${
                result._id
              }</a>`,
              textBody: `Email Confirmation Link ${hostUrl(req)}/api/confirmEmail/${
                result._id
              } `,
            });
      
            if (email.body.Messages[0].Status === "success") {
              logger.info(
                `CONFIRMATION EMAIL SEND | SUCCESS | ${
                  result.email
                } | ${name} | ${JSON.stringify(email.body.Messages[0])}`,
              );
            } else {
              logger.error(
                `CONFIRMATION EMAIL FAILED | FAILED | ${
                  result.email
                } | ${name} | ${JSON.stringify(email.body.Messages[0])}`,
              );
            }
          }
     

      return res
        .status(200)
        .json({ message: resmessage.record_added, id: result._id });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  checkEmailPhone: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: UserDto = req.body;

      if (data.password !== data.confirmPassword) {
        return res.status(400).json({
          message: resmessage.confirm_password_mismatch,
        });
      }

      let check;

      if(data.email) {
        check = await UserModel.findOne({
          $or: [
            { email: data.email.trim() },
            { phoneNumber: data.phoneNumber.trim() },
          ],
        });
      } else {
        check = await UserModel.findOne({
          $or: [
            { phoneNumber: data.phoneNumber.trim() },
          ],
        });
      }

      

      if (check) {
        return res.status(400).json({
          message: resmessage.username_email_exists,
        });
      }

      return res.status(200).json({ message: "okay" });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  login: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: LoginDto = req.body;

      let user: IUserModelSchema;
      if (isEmail(data.username)) {
        user = await UserModel.findOne({ email: data.username }).lean();
      } else {
        user = await UserModel.findOne({ phoneNumber: data.username }).lean();
      }

      if (!user) {
        return res.status(400).json({
          message: resmessage.bad_credentials,
        });
      }

      const checkPassword = await bcrypt.compare(data.password, user.password);
      if (!checkPassword) {
        return res.status(400).json({
          message: resmessage.bad_credentials,
        });
      }

      if (!user.isActive) {
        return res.status(400).json({
          message: resmessage.user_not_active,
        });
      }

      if(user.email) {
        if (!user.isEmailConfirm) {
          return res.status(400).json({
            message: resmessage.email_not_confirm,
          });
        }
  
      }

      return res.status(200).json({
        ...createToken(user._id, user.userType),
        result: { ...user, password: "" },
      });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  updateUser: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let data: UserDto = req.body;

      if (req.userType !== UserTypeEnum.ADMIN && req.userId !== id) {
        return res.status(400).json({
          message: resmessage.un_authorized_access,
        });
      }

      const source = await UserModel.findById(id);
      const documentsRemoved = removedfromArray(
        source.documentsUrl,
        data.documentsUrl,
      );

      if (documentsRemoved.length > 0) {
        const docIds = documentsRemoved.map((d) =>
          getPublicIdFromUrl(d.toString()),
        );
        await cloudinary.api.delete_resources(docIds);
      }

      if (
        source.profilePicUrl !== "" &&
        source.profilePicUrl !== null &&
        data.profilePicUrl !== "" &&
        source.profilePicUrl !== data.profilePicUrl
      ) {
        const profilePic = getPublicIdFromUrl(source.profilePicUrl);
        await cloudinary.api.delete_resources([profilePic]);
      }

      const user = await UserModel.findOneAndUpdate(
        { _id: id },
        { ...data, password: source.password },
        { new: true },
      ).lean();

      return res.status(200).json({
        message: resmessage.record_updated,
        result: { ...user, password: "" },
      });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  confirmEmail: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      await UserModel.findOneAndUpdate(
        { _id: id },
        { isEmailConfirm: true },
        { new: true },
      );
      return res.redirect(`${fronendUrl}/email-confirmed`);
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  forgetPassword: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: ForgetPasswordDto = req.body;

      let user: IUserModelSchema;
      if (isEmail(data.username)) {
        user = await UserModel.findOne({ email: data.username });
      } else {
        user = await UserModel.findOne({ phoneNumber: data.username });
      }
      user.update();
      if (!user) {
        return res.status(400).json({
          message: resmessage.user_not_found,
        });
      }

      if (!user.isActive) {
        return res.status(400).json({
          message: resmessage.user_not_active,
        });
      }

      if(user.email) {
        if (!user.isEmailConfirm) {
          return res.status(400).json({
            message: resmessage.email_not_confirm,
          });
        }
      }


      if (user.userType === UserTypeEnum.ADMIN) {
        return res.status(400).json({
          message: resmessage.un_authorized_access,
        });
      }

      const newPassword = randomPassword({ length: 9 });
      const name =
        user.userType === UserTypeEnum.SP_COM ||
        user.userType === UserTypeEnum.SR_COM
          ? user.contactPerson
          : `${user.firstName} ${user.lastName}`;

      const email = await sendEmail({
        toEmail: user.email,
        toName: name,
        subject: "New Password",
        htmlBody: `<b>New Password: </b> ${newPassword}`,
        textBody: `New Password: ${newPassword}`,
      });

      user.password = await bcrypt.hash(newPassword, 12);
      user.forgetPassword = true;
      await user.save();
      return res
        .status(200)
        .json({ message: resmessage.new_password_generated });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  changePassword: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.userId;
      let data: ChangePasswordDto = req.body;

      if (data.newPassword !== data.confirmPassword) {
        return res.status(400).json({
          message: resmessage.confirm_password_mismatch,
        });
      }

      const user = await UserModel.findById(id);

      const checkPassword = await bcrypt.compare(
        data.currentPassword,
        user.password,
      );
      if (!checkPassword) {
        return res.status(400).json({
          message: resmessage.incorrect_password,
        });
      }

      user.password = await bcrypt.hash(data.newPassword, 12);
      user.forgetPassword = false;
      await user.save();
      return res.status(200).json({ message: resmessage.record_updated });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  changeIsActive: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let active = req.params.active;

      await UserModel.findOneAndUpdate(
        { _id: id },
        { isActive: active === "true" ? true : false },
        { new: true },
      );
      return res.status(200).json({ message: resmessage.record_updated });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  changeUserJobs: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let jobs = req.params.jobs;

      await UserModel.findOneAndUpdate(
        { _id: id },
        { jobsCanBuy: parseInt(jobs, 10) },
        { new: true },
      );
      return res.status(200).json({ message: resmessage.record_updated });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  getUserInfo: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.userId;

      let result = await UserModel.findById(id);
      if (!result) {
        return res.status(400).json({
          message: resmessage.bad_credentials,
        });
      }
      return res.status(200).json({ result });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  getUsers: async (req: RequestWithUserID, res: Response) => {
    try {
      const rq = qs.parse(url.parse(req.url).query);

      let query = UserModel.find().lean();

      if (rq.isActive) query.where({ isActive: rq.isActive });
      if (rq.id) query.where({ _id: rq.id });
      if (rq.type) query.where({ userType: rq.type });
      if (rq.skillId) query.where({ skills: { $in: [rq.skillId] } });
      if (rq.name)
        query.where({
          $or: [
            {
              firstName: { $regex: new RegExp(rq.name.toString().trim(), "i") },
            },
            {
              lastName: { $regex: new RegExp(rq.name.toString().trim(), "i") },
            },
            {
              companyName: { $regex: new RegExp(rq.name.toString().trim(), "i") },
            },
            {
              contactPerson: { $regex: new RegExp(rq.name.toString().trim(), "i") },
            },
          ],
        });

      const totalQuery = UserModel.find(query).countDocuments();

      if (rq.limit) query.limit(parseInt(rq.limit.toString(), 10));
      if (rq.skip) query.skip(parseInt(rq.skip.toString(), 10));

      const total = await totalQuery.count();
      const result = await query;

      return res.status(200).json({ result, total });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },
};

export default UserController;
