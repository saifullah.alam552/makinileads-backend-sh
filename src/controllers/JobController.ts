import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { Response } from "express";
import qs from "qs";
import url from "url";
import { JobDto, JobPurchaseDto, JobStatusDto } from "../dto/JobDto";
import { RequestWithUserID } from "../interfaces";
import logger from "../logger";
import { resmessage } from "../resmessage";
import { JobModel, JobPurchaseType, JobStatusEnum } from "../schema/JobModel";
import { UserModel, UserTypeEnum } from "../schema/UserModel";

dayjs.extend(relativeTime);

const JobController = {
  addJob: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: JobDto = req.body;

      if (
        req.userType === UserTypeEnum.SP_COM ||
        req.userType === UserTypeEnum.SP_IND
      ) {
        return res
          .status(400)
          .json({ message: resmessage.not_allowed_to_create_job });
      }

      const result = await new JobModel(data).save();
      return res
        .status(200)
        .json({ message: resmessage.record_added, id: result._id });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  editJob: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let data: JobDto = req.body;

      if (
        req.userType === UserTypeEnum.SP_COM ||
        req.userType === UserTypeEnum.SP_IND
      ) {
        return res
          .status(400)
          .json({ message: resmessage.un_authorized_access });
      }

      const job = await JobModel.findById(id);

      if (job.jobPurchaseType !== JobPurchaseType.NONE) {
        return res
          .status(400)
          .json({ message: resmessage.job_purchashed_edit });
      }

      const update = await JobModel.findByIdAndUpdate(
        { _id: id },
        {
          ...data,
        },
        { new: true },
      )
        .populate({
          path: "postedBy",
          select: "-password",
        })
        .lean();

      return res
        .status(200)
        .json({ message: resmessage.record_updated, result: { ...update } });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  deleteJob: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;

      if (
        req.userType === UserTypeEnum.SP_COM ||
        req.userType === UserTypeEnum.SP_IND
      ) {
        return res
          .status(400)
          .json({ message: resmessage.un_authorized_access });
      }

      const job = await JobModel.findById(id);

      if (job.jobPurchaseType !== JobPurchaseType.NONE) {
        return res
          .status(400)
          .json({ message: resmessage.job_purchashed_delete });
      }

      await JobModel.findOneAndDelete({ _id: id });
      return res.status(200).json({ message: resmessage.record_deleted });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  getJobs: async (req: RequestWithUserID, res: Response) => {
    try {
      const rq = qs.parse(url.parse(req.url).query);

      const query = JobModel.find().lean();

      if (rq.id) query.where({ _id: rq.id });
      if (rq.isActive) query.where({ isActive: rq.isActive });
      if (rq.skillId) query.where({ skills: { $in: [rq.skillId] } });
      if (rq.status) query.where({ jobStatus: rq.status });
      if (rq.purchaseType) query.where({ jobPurchaseType: rq.purchaseType });
      if (rq.postedById) query.where({ postedBy: rq.postedById });
      if (rq.purchasedById)
        query.where({ purchasedBy: { $in: [rq.purchasedById] } });
      if (rq.postedBy)
        query.populate({
          path: "postedBy",
          select: "-password",
        });
      if (rq.purchasedBy)
        query.populate({
          path: "purchasedBy",
          select: "-password",
        });
      if (rq.summary) query.select("-streetAddress -postedBy");

      if (rq.sortBy) {
        query.sort(rq.sortBy);
      } else {
        query.sort("-createdAt");
      }

      if (rq.postedDateStart && rq.postedDateEnd) {
        query.where({
          createdAt: { $gte: rq.postedDateStart, $lte: rq.postedDateEnd },
        });
      }

      const totalQuery = JobModel.find(query).countDocuments();

      if (rq.limit) query.limit(parseInt(rq.limit.toString(), 10));
      if (rq.skip) query.skip(parseInt(rq.skip.toString(), 10));

      const total = await totalQuery.count();
      const result = await query;

      return res.status(200).json({ result, total });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  changeJobStatus: async (req: RequestWithUserID, res: Response) => {
    try {
      let userType = req.userType;
      let jobId = req.params.id;
      let data: JobStatusDto = req.body;

      const job = await JobModel.findById(jobId);

      switch (userType) {
        case UserTypeEnum.ADMIN:
          await JobModel.findOneAndUpdate(
            { _id: jobId },
            {
              isActive: data.isActive,
              jobStatus: data.jobStatus,
              jobPurchaseType:
                data.jobStatus === JobStatusEnum.REPOSTED
                  ? JobPurchaseType.NONE
                  : job.jobPurchaseType,
              purchasedBy: JobStatusEnum.REPOSTED ? [] : job.purchasedBy,
            },
            { new: true },
          );
          return res.status(200).json({ message: resmessage.record_updated });

        case UserTypeEnum.SR_COM:
        case UserTypeEnum.SR_IND:
          await JobModel.findOneAndUpdate(
            { _id: jobId },
            {
              isActive: data.isActive,
              jobStatus:
                data.jobStatus === JobStatusEnum.COMPLETED
                  ? JobStatusEnum.COMPLETED
                  : data.jobStatus === JobStatusEnum.CONTACTED
                  ? JobStatusEnum.CONTACTED
                  : job.jobStatus,
            },
            { new: true },
          );
          return res.status(200).json({ message: resmessage.record_updated });

        case UserTypeEnum.SP_COM:
        case UserTypeEnum.SP_IND:
          return res.status(400).json({
            message: resmessage.un_authorized_access,
          });

        default:
          return res.status(400).json({
            message: resmessage.something_wrong,
          });
      }
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  purchaseJob: async (req: RequestWithUserID, res: Response) => {
    try {
      if (
        req.userType === UserTypeEnum.ADMIN ||
        req.userType === UserTypeEnum.SR_COM ||
        req.userType === UserTypeEnum.SR_IND
      ) {
        return res
          .status(400)
          .json({ message: resmessage.not_service_provider });
      }

      const user = await UserModel.findById(req.userId);

      if (!user.isActive) {
        return res.status(400).json({
          message: resmessage.user_not_active,
        });
      }

      let data: JobPurchaseDto = req.body;

      const job = await JobModel.findById(data.jobId);

      if (job && !job.isActive) {
        return res.status(400).json({
          message: resmessage.job_not_active,
        });
      }

      if (
        job.jobStatus === JobStatusEnum.COMPLETED ||
        job.jobStatus === JobStatusEnum.CONTACTED
      ) {
        return res.status(400).json({
          message: resmessage.job_already_contacted_completed,
        });
      }

      // check max jobs user can buy.
      const openJobCount = await JobModel.find({
        $and: [
          { purchasedBy: { $in: [req.userId] } },
          { jobStatus: JobStatusEnum.PURCHASED },
        ],
      }).countDocuments();

      if (user.jobsCanBuy === openJobCount) {
        return res.status(400).json({ message: resmessage.max_job_purchased });
      }

      // purchase job
      if (job.jobPurchaseType === JobPurchaseType.NONE) {
        job.jobPurchaseType = data.jobPurchaseType;
        job.jobStatus = JobStatusEnum.PURCHASED;
        job.purchasedBy.push(user._id);

        await job.save();

        // const expiryDate = dayjs(new Date()).add(48, "hour").toDate();
        // const nsData = { userId: user._id, jobId: job._id };
        // ns.scheduleJob(expiryDate, scheduleJobTimeLimt(nsData));

        return res.status(200).json({
          message: resmessage.record_updated,
        });
      }

      if (job.jobPurchaseType === JobPurchaseType.GENERAL) {
        if (job.purchasedBy.length < 3 && !job.purchasedBy.includes(user._id)) {
          job.purchasedBy.push(user._id);
          await job.save();

          // const expiryDate = dayjs(new Date()).add(48, "hour").toDate();
          // const nsData = { userId: user._id, jobId: job._id };
          // ns.scheduleJob(expiryDate, scheduleJobTimeLimt(nsData));

          return res.status(200).json({
            message: resmessage.record_updated,
          });
        } else {
          return res.status(400).json({
            message: resmessage.general_purchase_limit,
          });
        }
      }
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },
};

function scheduleJobTimeLimt(nsData: any) {
  return async function (data: any) {
    try {
      logger.info(
        `RUNNING | SCHEDULED JOB | JOB_ID ${data.jobId} | USER_ID ${data.userId}`,
      );
      const job = await JobModel.findById(data.jobId);

      if (job.jobStatus === JobStatusEnum.PURCHASED) {
        if (job.jobPurchaseType === JobPurchaseType.EXCLUSIVE) {
          job.purchasedBy = [];
          job.jobStatus = JobStatusEnum.REPOSTED;
          job.jobPurchaseType = JobPurchaseType.NONE;
          await job.save();
        }

        if (job.jobPurchaseType === JobPurchaseType.GENERAL) {
          let users = job.purchasedBy;

          job.purchasedBy = users.filter(
            (d: string) => d.toString() !== data.userId.toString(),
          );

          if (job.purchasedBy.length === 0) {
            job.jobStatus = JobStatusEnum.REPOSTED;
            job.jobPurchaseType = JobPurchaseType.NONE;
          }

          await job.save();
        }

        logger.info(
          `SUCCESSFUL | SCHEDULED JOB | JOB_ID ${data.jobId} | USER_ID ${data.userId}`,
        );
      }
    } catch (err) {
      logger.error(
        `FAILED | SCHEDULED JOB | JOB_ID ${data.jobId} | USER_ID ${data.userId}`,
      );
      logger.error(err.toString());
    }
  }.bind(null, nsData);
}

export default JobController;
