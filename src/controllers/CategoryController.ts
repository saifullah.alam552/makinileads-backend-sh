import { Response } from "express";
import qs from "qs";
import url from "url";
import { CategoryDto, SubCategoryDto } from "../dto/CategoryDto";
import { RequestWithUserID } from "../interfaces";
import logger from "../logger";
import { resmessage } from "../resmessage";
import { CategoryModel } from "../schema/CategoryModel";
import { SubCategoryModel } from "../schema/SubCategoryModel";

const CategoryController = {
  // CATEGORIES
  addCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: CategoryDto = req.body;

      const check = await CategoryModel.findOne({
        title: { $regex: new RegExp(data.title.trim(), "i") },
      });

      if (check) {
        return res.status(400).json({
          message: resmessage.record_exists,
        });
      }

      const result = await new CategoryModel(data).save();
      return res
        .status(200)
        .json({ message: resmessage.record_added, id: result._id });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  editCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let data: CategoryDto = req.body;

      const check = await CategoryModel.findOne({
        $and: [
          { _id: { $ne: id } },
          { title: { $regex: new RegExp(data.title.trim(), "i") } },
        ],
      });

      if (check) {
        return res.status(400).json({
          message: resmessage.record_exists,
        });
      }

      const update = await CategoryModel.findOneAndUpdate(
        { _id: id },
        { ...data },
        { new: true },
      ).lean();

      return res
        .status(200)
        .json({ message: resmessage.record_updated, result: { ...update } });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  deleteCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      await CategoryModel.findOneAndDelete({ _id: id });
      return res.status(200).json({ message: resmessage.record_deleted });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  getCategories: async (req: RequestWithUserID, res: Response) => {
    try {
      const rq = qs.parse(url.parse(req.url).query);
      let query = CategoryModel.find().lean();

      if (rq.isActive) query.where({ isActive: rq.isActive });
      if (rq.id) query.where({ _id: rq.id });
      if (rq.title)
        query.where({
          title: { $regex: new RegExp(rq.title.toString(), "i") },
        });

      const result = await query;
      return res.status(200).json({ result });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  // SUB CATEGORIES
  addSubCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let data: SubCategoryDto = req.body;

      const check = await SubCategoryModel.findOne({
        title: { $regex: new RegExp(data.title.trim(), "i") },
      });

      if (check) {
        return res.status(400).json({
          message: resmessage.record_exists,
        });
      }

      const result = await new SubCategoryModel(data).save();
      return res
        .status(200)
        .json({ message: resmessage.record_added, id: result._id });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  editSubCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      let data: SubCategoryDto = req.body;

      const check = await SubCategoryModel.findOne({
        $and: [
          { _id: { $ne: id } },
          { title: { $regex: new RegExp(data.title.trim(), "i") } },
        ],
      });

      if (check) {
        return res.status(400).json({
          message: resmessage.record_exists,
        });
      }

      const update = await SubCategoryModel.findOneAndUpdate(
        { _id: id },
        { ...data },
        { new: true },
      )
        .populate({ path: "category" })
        .lean();

      return res
        .status(200)
        .json({ message: resmessage.record_updated, result: { ...update } });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  deleteSubCategory: async (req: RequestWithUserID, res: Response) => {
    try {
      let id = req.params.id;
      await SubCategoryModel.findOneAndDelete({ _id: id });
      return res.status(200).json({ message: resmessage.record_deleted });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },

  getSubCategories: async (req: RequestWithUserID, res: Response) => {
    try {
      const rq = qs.parse(url.parse(req.url).query);
      let query = SubCategoryModel.find().lean();

      if (rq.isActive) query.where({ isActive: rq.isActive });
      if (rq.id) query.where({ _id: rq.id });
      if (rq.title)
        query.where({
          title: { $regex: new RegExp(rq.title.toString(), "i") },
        });
      if (rq.withCategory) query.populate({ path: "category" });

      const result = await query;
      return res.status(200).json({ result });
    } catch (err) {
      logger.error(err.toString());
      return res
        .status(400)
        .json({ message: resmessage.something_wrong, error: err.toString() });
    }
  },
};

export default CategoryController;
