import createMollieClient from "@mollie/api-client";
import * as jwt from "jsonwebtoken";
import { connect } from "node-mailjet";
import { DataStoredInToken, RequestWithUserID, TokenData } from "./interfaces";
import logger from "./logger";
import { UserTypeEnum } from "./schema/UserModel";
import cloudinaryApi from "cloudinary";

export const JWT_SECRET =
  process.env.JWT_SECRET || "8h8Q80yq1DpumUntkQxqP2whHyl4gfS3";

export const hostUrl = (req: RequestWithUserID) =>
  `${req.protocol}://${req.get("host")}`;

export const fronendUrl =
  process.env.NODE_ENV === "production"
    ? "https://makini.ahsanghalib.vercel.app"
    : "http://localhost:3000";

export const MONGODB_URI =
  "mongodb+srv://Makini:12345@cluster0.ieeom.mongodb.net/makini?retryWrites=true&w=majority";

export const cloudinary = cloudinaryApi.v2;
cloudinary.config({
  cloud_name: "drhyt23ny",
  api_key: "667253477821582",
  api_secret: "zLif62lt_3L7e2IqHRkXYiV1YYA",
  enhance_image_tag: true,
  static_file_support: true,
});

export const mollieClient = createMollieClient({
  apiKey: "test_aTJjmwNGuCrcR5Cq7BKrgTFrRbBTej",
});

export const mailjet = connect(
  "55e9ef4cc15a09ea12bac4c58c30c6ee",
  "67ec30e000ae510d42655c0d971c0c69",
);

export const sendEmail = (data: {
  toEmail: string;
  toName: string;
  subject: string;
  textBody: string;
  htmlBody: string;
}) =>
  mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "makinileads@gmail.com",
          Name: "Makini Leads",
        },
        To: [
          {
            Email: data.toEmail,
            Name: data.toName,
          },
        ],
        Subject: data.subject,
        TextPart: data.textBody,
        HTMLPart: data.htmlBody,
      },
    ],
  });

export function createToken(userId: string, userType: UserTypeEnum): TokenData {
  const expiresIn = 3600 * 24 * 24;
  const secret = JWT_SECRET;
  const dataStoredInToken: DataStoredInToken = {
    userId: userId,
    userType: userType,
  };

  const token = jwt.sign(dataStoredInToken, secret, {
    expiresIn,
    algorithm: "HS512",
    issuer: "MakiniLeads",
    noTimestamp: true,
  });

  const accessToken = "Bearer " + token;
  logger.info(`JWT_TOKEN_CREATED | ${userId} |${accessToken}`);
  return { expiresIn, accessToken };
}

export const removedfromArray = (
  source: string[],
  dest: string[],
): string[] => [...source.filter((x) => !dest.includes(x))];

export const getPublicIdFromUrl = (url: string): string => {
  let fileName: string[] = url.split("/")[url.split("/").length - 1].split(".");
  if (fileName.length > 2) {
    return fileName.slice(0, fileName.length - 1).join(".");
  } else {
    return fileName[0];
  }
};
