import { plainToClass } from "class-transformer";
import { ClassType } from "class-transformer/ClassTransformer";
import { validate, ValidationError } from "class-validator";
import { NextFunction, Request, RequestHandler, Response } from "express";
import * as jwt from "jsonwebtoken";
import { DataStoredInToken, RequestWithUserID } from "./interfaces";
import logger from "./logger";
import { resmessage } from "./resmessage";
import { UserTypeEnum } from "./schema/UserModel";
import { JWT_SECRET } from "./utils";

export function dtoValid<T>(
  type: ClassType<T>,
  skipMissingProperties = false,
): RequestHandler {
  return function (req: Request, res: Response, next: NextFunction) {
    const message: string[] = [];
    validate(plainToClass(type, req.body), { skipMissingProperties })
      .then((errors: ValidationError[]) => {
        if (errors.length > 0) {
          errors.map((error: ValidationError) => {
            message.push(Object.values(error.constraints).join(", "));
          });
          return res.status(400).json({
            message: resmessage.dto_validation_error,
            error: message,
          });
        } else {
          next();
        }
      })
      .catch((err: any) => {
        logger.warn(`DTO VALIDATION ERROR | ${err}`);
        return res
          .status(400)
          .json({ message: resmessage.dto_validation_error, error: message });
      });
  };
}

export function isAuth(
  req: RequestWithUserID,
  res: Response,
  next: NextFunction,
) {
  if (
    req.path === "/login" ||
    req.path === "/register" ||
    req.path === "/forget-password"
  ) {
    return next();
  }

  const authHeader = req.get("Authorization");
  const secret = JWT_SECRET;

  if (!authHeader) {
    logger.warn(`UN-AUTHORIZED ACCESS | ${req.path}`);
    return res.status(401).json({
      message: resmessage.un_authorized_access,
    });
  }
  const token = authHeader.split(" ")[1];

  let decodedToken: any;

  try {
    decodedToken = jwt.verify(token, secret) as DataStoredInToken;
  } catch (err) {
    logger.warn(`UN-AUTHORIZED ACCESS | TOKEN ERROR | ${req.path}`);
    return res
      .status(401)
      .json({ message: resmessage.something_wrong, error: err });
  }

  if (!decodedToken) {
    logger.warn(`UN-AUTHORIZED ACCESS | TOKEN ERROR | ${req.path}`);
    return res.status(401).json({
      message: resmessage.un_authorized_access,
    });
  }

  req.userId = decodedToken.userId;
  req.userType = decodedToken.userType;

  next();
}

export function isAdmin(
  req: RequestWithUserID,
  res: Response,
  next: NextFunction,
) {
  if (req.userType !== UserTypeEnum.ADMIN) {
    return res.status(401).json({
      message: resmessage.un_authorized_access,
    });
  }

  next();
}

export function headers(req: Request, res: Response, next: NextFunction) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE",
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, Authorization",
  );
  if (req.method === "OPTIONS") {
    return res.status(200).end();
  }
  next();
}
